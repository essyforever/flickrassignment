package eman.ca.flickrassignment;

import android.app.Application;
import android.content.Context;

import io.appflate.restmock.android.RESTMockTestRunner;

/**
 * Created by Esmond on 2018-09-14.
 */

public class MockRunner extends RESTMockTestRunner {

    @Override
    public Application newApplication(ClassLoader cl,
                                      String className,
                                      Context context) throws InstantiationException, IllegalAccessException, ClassNotFoundException {

        //I'm changing the application class only for test purposes. there I'll instantiate AppModule with RESTMock's url.
        String testApplicationClassName = TestApplication.class.getCanonicalName();
        return super.newApplication(cl, testApplicationClassName, context);
    }
}
