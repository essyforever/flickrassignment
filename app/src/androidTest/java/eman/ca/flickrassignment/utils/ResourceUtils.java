package eman.ca.flickrassignment.utils;

import android.app.Activity;
import android.content.Context;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Esmond on 2018-09-14.
 */

public class ResourceUtils {

    public static String loadFileToString(Context context, String path) {
        try {
            InputStream is = context.getResources().getAssets().open(path);
            final int bufferLen = 64 * 1024;

            byte[] buffer = new byte[bufferLen];

            ByteArrayOutputStream os = new ByteArrayOutputStream(bufferLen);

            int readLen = is.read(buffer);

            while (readLen > 0) {
                os.write(buffer, 0, readLen);
                readLen = is.read(buffer);
            }

            byte[] bytes = os.toByteArray();
            return new String(bytes);

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
