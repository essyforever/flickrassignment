package eman.ca.flickrassignment.view.activity.actionmethod;

import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.TextView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;

import eman.ca.flickrassignment.R;
import eman.ca.flickrassignment.model.FlickrPhoto;
import eman.ca.flickrassignment.view.viewholder.FlickrViewHolder;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

/**
 * Created by Esmond on 2018-09-15.
 */

public class MainActivityActions {

    public void searchByTagTest(String textEntered) {
        ViewInteraction actionMenuItemView = onView(
                allOf(withId(R.id.menu_action_search), withContentDescription("Search"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.action_bar),
                                        1),
                                0),
                        isDisplayed()));
        actionMenuItemView.perform(click());

        ViewInteraction searchAutoComplete = onView(
                allOf(withId(R.id.search_src_text),
                        childAtPosition(
                                allOf(withId(R.id.search_plate),
                                        childAtPosition(
                                                withId(R.id.search_edit_frame),
                                                1)),
                                0),
                        isDisplayed()));
        searchAutoComplete.perform(replaceText(textEntered), closeSoftKeyboard());

        ViewInteraction searchAutoComplete2 = onView(
                allOf(withId(R.id.search_src_text), withText(textEntered),
                        childAtPosition(
                                allOf(withId(R.id.search_plate),
                                        childAtPosition(
                                                withId(R.id.search_edit_frame),
                                                1)),
                                0),
                        isDisplayed()));
        searchAutoComplete2.perform(pressImeActionButton());

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }

    public void checkIfFlickrPhotoLoaded(FlickrPhoto flickrPhoto) {
        onView(withId(R.id.recyclerView)).perform(RecyclerViewActions.scrollToHolder(isMatchingItem(flickrPhoto)));
        onView(withText(flickrPhoto.getTitle())).check(matches(isDisplayed()));
    }

    private static Matcher<FlickrViewHolder> isMatchingItem(final FlickrPhoto flickrPhoto) {
        return new TypeSafeDiagnosingMatcher<FlickrViewHolder>() {
            @Override
            protected boolean matchesSafely(FlickrViewHolder item, Description mismatchDescription) {
                return flickrPhoto.getTitle().equals(((TextView)item.itemView.findViewById(R.id.title)).getText());
            }

            @Override
            public void describeTo(Description description) {
            }
        };
    }
}
