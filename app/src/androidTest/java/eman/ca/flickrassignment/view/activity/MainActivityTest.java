package eman.ca.flickrassignment.view.activity;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import eman.ca.flickrassignment.R;
import eman.ca.flickrassignment.model.ResultList;
import eman.ca.flickrassignment.utils.ResourceUtils;
import eman.ca.flickrassignment.view.activity.actionmethod.MainActivityActions;
import io.appflate.restmock.RESTMockServer;
import io.appflate.restmock.utils.RequestMatchers;
import okhttp3.mockwebserver.MockResponse;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.AllOf.allOf;

/**O
 * Created by Esmond on 2018-09-15.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityTest {

    private MainActivityActions mMainActivityActions;
    private static final String RESPONSE = "response.json";
    private static final String RESPONSE_TAGS = "response_with_tags.json";

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class, true, false);

    @Before
    public void setup() throws Exception {
        RESTMockServer.reset();
        mMainActivityActions = new MainActivityActions();
    }

    @Test
    public void RecyclerViewTest() {
        Gson gson = new Gson();
        ResultList resultList = gson.fromJson(ResourceUtils.loadFileToString(InstrumentationRegistry.getContext(),RESPONSE), ResultList.class);
        RESTMockServer.whenGET(RequestMatchers.isGET()).thenReturnFile(RESPONSE);

        mActivityRule.launchActivity(new Intent());

        onView(ViewMatchers.withId(R.id.recyclerView)).check(matches(isDisplayed()));
        onView(withId(R.id.progressBar)).check(matches(not(isDisplayed())));

        // Check first item
        mMainActivityActions.checkIfFlickrPhotoLoaded(resultList.getFlickrPhoto(0));
        // Check middle item
        mMainActivityActions.checkIfFlickrPhotoLoaded(resultList.getFlickrPhoto(resultList.getFlickrPhotoList().size()/2));
        // Check last item
        mMainActivityActions.checkIfFlickrPhotoLoaded(resultList.getFlickrPhoto(resultList.getFlickrPhotoList().size()-1));
    }

    @Test
    public void NetworkErrorWithRetryTest() {
        Gson gson = new Gson();
        ResultList resultList = gson.fromJson(ResourceUtils.loadFileToString(InstrumentationRegistry.getContext(),RESPONSE), ResultList.class);
        RESTMockServer.whenGET(RequestMatchers.isGET()).thenReturn(new MockResponse().setResponseCode(404));
        mActivityRule.launchActivity(new Intent());

        onView(withId(R.id.recyclerView)).check(matches(not(isDisplayed())));
        onView(withId(R.id.progressBar)).check(matches(isDisplayed()));
        onView(allOf(withId(android.support.design.R.id.snackbar_text), withText(mActivityRule.getActivity().getResources().getString(R.string.error_network_please_try_again))))
                .check(matches(isDisplayed()));

        RESTMockServer.reset();
        RESTMockServer.whenGET(RequestMatchers.isGET()).thenReturnFile(RESPONSE);

        onView(withId(android.support.design.R.id.snackbar_action)).check(matches(isDisplayed())).perform(click());

        // Check first item
        mMainActivityActions.checkIfFlickrPhotoLoaded(resultList.getFlickrPhoto(0));
    }

    @Test
    public void checkSearchByTagTest() {
        String tags = "test123";
        Gson gson = new Gson();
        ResultList resultList = gson.fromJson(ResourceUtils.loadFileToString(InstrumentationRegistry.getContext(),RESPONSE_TAGS), ResultList.class);
        RESTMockServer.whenGET(RequestMatchers.pathContains("tags="+tags)).thenReturnFile(RESPONSE_TAGS);

        mActivityRule.launchActivity(new Intent());

        mMainActivityActions.searchByTagTest(tags);

        // Check first item
        mMainActivityActions.checkIfFlickrPhotoLoaded(resultList.getFlickrPhoto(0));
        // Check middle item
        mMainActivityActions.checkIfFlickrPhotoLoaded(resultList.getFlickrPhoto(resultList.getFlickrPhotoList().size()/2));
        // Check last item
        mMainActivityActions.checkIfFlickrPhotoLoaded(resultList.getFlickrPhoto(resultList.getFlickrPhotoList().size()-1));
    }
}
