package eman.ca.flickrassignment;

import eman.ca.flickrassignment.network.DaggerNetworkComponent;
import eman.ca.flickrassignment.network.NetworkModule;
import io.appflate.restmock.RESTMockServer;

/**
 * Created by Esmond on 2018-09-14.
 */

public class TestApplication extends FlckrApplication {

    @Override
    protected void setupGraph() {
        sNetworkComponent = DaggerNetworkComponent.builder()
                .networkModule(new NetworkModule(RESTMockServer.getUrl()))
                .build();
    }
}
