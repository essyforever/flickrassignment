package eman.ca.flickrassignment.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Esmond on 2018-09-15.
 */

public class ResourceUtils {
    public static String loadFileToString(String path) {
        try {
            InputStream is = ResourceUtils.class.getClassLoader().getResourceAsStream(path);
            final int bufferLen = 64 * 1024;

            byte[] buffer = new byte[bufferLen];

            ByteArrayOutputStream os = new ByteArrayOutputStream(bufferLen);

            int readLen = is.read(buffer);

            while (readLen > 0) {
                os.write(buffer, 0, readLen);
                readLen = is.read(buffer);
            }

            byte[] bytes = os.toByteArray();
            return new String(bytes);

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
