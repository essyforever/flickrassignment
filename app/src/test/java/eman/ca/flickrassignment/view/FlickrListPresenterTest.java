package eman.ca.flickrassignment.view;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;

import eman.ca.flickrassignment.R;
import eman.ca.flickrassignment.model.ResultList;
import eman.ca.flickrassignment.utils.ResourceUtils;
import eman.ca.flickrassignment.view.activity.MainActivity;
import eman.ca.flickrassignment.view.adapter.FlickrListPresenter;
import eman.ca.flickrassignment.view.viewholder.FlickrViewHolder;

import static org.hamcrest.CoreMatchers.is;

/**
 * Created by Esmond on 2018-09-15.
 */
@RunWith(RobolectricTestRunner.class)
public class FlickrListPresenterTest {

    private static final String RESPONSE = "response.json";


    @Test
    public void onBindRowViewAtPosition_Test() {
        Picasso.setSingletonInstance(new Picasso.Builder(RuntimeEnvironment.application).build());

        Gson gson = new Gson();
        ResultList resultList = gson.fromJson(ResourceUtils.loadFileToString(RESPONSE), ResultList.class);

        Assert.assertThat(resultList.getFlickrPhotoList().size(), is(20));

        MainActivity mainActivity = Robolectric.setupActivity(MainActivity.class);
        ViewGroup viewGroup = mainActivity.findViewById(R.id.fragment_placeholder);
        FlickrViewHolder viewHolder = new FlickrViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_flickr_list_item, viewGroup, false));

        TextView title = viewHolder.itemView.findViewById(R.id.title);
        TextView date = viewHolder.itemView.findViewById(R.id.date_taken);
        TextView author = viewHolder.itemView.findViewById(R.id.author);
        TextView tags = viewHolder.itemView.findViewById(R.id.tags);

        FlickrListPresenter presenter = new FlickrListPresenter(resultList);
        for(int position=0; position<resultList.getFlickrPhotoList().size(); position++) {
            presenter.onBindRowViewAtPosition(position, viewHolder);

            Assert.assertThat(title.getText().toString(), is(resultList.getFlickrPhoto(position).getTitle()));
            Assert.assertThat(date.getText().toString(), is(resultList.getFlickrPhoto(position).getDateTaken()));
            Assert.assertThat(author.getText().toString(), is(resultList.getFlickrPhoto(position).getAuthor()));
            Assert.assertThat(tags.getText().toString(), is(viewGroup.getResources().getString(R.string.tags, resultList.getFlickrPhoto(position).getTags())));
        }
    }

    @Test
    public void getRowsCount_Test() {
        Gson gson = new Gson();
        ResultList resultList = gson.fromJson(ResourceUtils.loadFileToString(RESPONSE), ResultList.class);

        Assert.assertThat(resultList.getFlickrPhotoList().size(), is(20));

        {
            FlickrListPresenter presenter = new FlickrListPresenter(resultList);

            Assert.assertThat(presenter.getRowsCount(), is(resultList.getFlickrPhotoList().size()));
        }
        {
            resultList.setFlickrPhotoList(null);
            FlickrListPresenter presenter = new FlickrListPresenter(resultList);

            Assert.assertThat(presenter.getRowsCount(), is(0));
        }
    }
}
