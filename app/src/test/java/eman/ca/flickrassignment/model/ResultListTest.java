package eman.ca.flickrassignment.model;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.is;

/**
 * Created by Esmond on 2018-09-15.
 */
public class ResultListTest {

    @Test
    public void getFlickrPhoto_Test() {
        ResultList resultList = new ResultList();

        {
            Assert.assertNull(resultList.getFlickrPhoto(0));
        }
        {
            ArrayList<FlickrPhoto> flickrPhotoArrayList = new ArrayList<>();
            FlickrPhoto flickrPhoto = new FlickrPhoto();
            flickrPhotoArrayList.add(flickrPhoto);

            resultList.setFlickrPhotoList(flickrPhotoArrayList);

            Assert.assertNull(resultList.getFlickrPhoto(-1));
            Assert.assertNull(resultList.getFlickrPhoto(1));
            Assert.assertThat(resultList.getFlickrPhoto(0), is(flickrPhoto));
        }
    }
}
