package eman.ca.flickrassignment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Esmond on 2018-09-14.
 */

public class Media implements Serializable {

    @SerializedName("m")
    @Expose
    private String mMediaUrl;

    public String getMediaUrl() {
        return mMediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        mMediaUrl = mediaUrl;
    }
}
