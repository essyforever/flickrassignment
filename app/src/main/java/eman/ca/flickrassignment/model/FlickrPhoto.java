package eman.ca.flickrassignment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Esmond on 2018-09-14.
 */

public class FlickrPhoto implements Serializable {

    @SerializedName("title")
    @Expose
    private String mTitle;
    @SerializedName("link")
    @Expose
    private String mLink;
    @SerializedName("media")
    @Expose
    private Media mMedia;
    @SerializedName("date_taken")
    @Expose
    private String mDateTaken;
    @SerializedName("description")
    @Expose
    private String mDescription;
    @SerializedName("published")
    @Expose
    private String mPublished;
    @SerializedName("author")
    @Expose
    private String mAuthor;
    @SerializedName("author_id")
    @Expose
    private String mAuthorId;
    @SerializedName("tags")
    @Expose
    private String mTags;

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getLink() {
        return mLink;
    }

    public void setLink(String link) {
        mLink = link;
    }

    public Media getMedia() {
        return mMedia;
    }

    public void setMedia(Media media) {
        mMedia = media;
    }

    public String getDateTaken() {
        return mDateTaken;
    }

    public void setDateTaken(String dateTaken) {
        mDateTaken = dateTaken;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getPublished() {
        return mPublished;
    }

    public void setPublished(String published) {
        mPublished = published;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public void setAuthor(String author) {
        mAuthor = author;
    }

    public String getAuthorId() {
        return mAuthorId;
    }

    public void setAuthorId(String authorId) {
        mAuthorId = authorId;
    }

    public String getTags() {
        return mTags;
    }

    public void setTags(String tags) {
        mTags = tags;
    }
}
