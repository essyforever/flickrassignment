package eman.ca.flickrassignment.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Esmond on 2018-09-14.
 */

public class ResultList {

    @SerializedName("items")
    @Expose
    private List<FlickrPhoto> mFlickrPhotoList = null;

    public List<FlickrPhoto> getFlickrPhotoList() {
        return mFlickrPhotoList;
    }

    public void setFlickrPhotoList(List<FlickrPhoto> flickrPhotoList) {
        mFlickrPhotoList = flickrPhotoList;
    }

    public FlickrPhoto getFlickrPhoto(int position) {
        if(mFlickrPhotoList != null && position < mFlickrPhotoList.size() && position >= 0) {
            return mFlickrPhotoList.get(position);
        }
        return null;
    }
}
