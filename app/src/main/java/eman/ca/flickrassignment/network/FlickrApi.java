package eman.ca.flickrassignment.network;

import eman.ca.flickrassignment.model.ResultList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Esmond on 2018-09-14.
 */

public interface FlickrApi {

    @GET("feeds/photos_public.gne?format=json&nojsoncallback=1")
    Call<ResultList> getPublicPhotos(@Query("tags") String tag);
}
