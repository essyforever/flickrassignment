package eman.ca.flickrassignment.network;

import javax.inject.Singleton;

import dagger.Component;
import eman.ca.flickrassignment.view.fragment.FlickrResultFragment;

/**
 * Created by Esmond on 2018-09-14.
 */
@Singleton
@Component(modules = {NetworkModule.class})
public interface NetworkComponent {

    FlickrApi getFlickrApi();

    void inject(FlickrResultFragment fragment);
}
