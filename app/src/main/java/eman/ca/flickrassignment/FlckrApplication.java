package eman.ca.flickrassignment;

import android.app.Application;

import eman.ca.flickrassignment.network.DaggerNetworkComponent;
import eman.ca.flickrassignment.network.NetworkComponent;
import eman.ca.flickrassignment.network.NetworkModule;
import eman.ca.flickrassignment.utils.Const;

/**
 * Created by Esmond on 2018-09-14.
 */

public class FlckrApplication extends Application {

    static NetworkComponent sNetworkComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        setupGraph();
    }

    protected void setupGraph() {
        sNetworkComponent = DaggerNetworkComponent.builder()
                .networkModule(new NetworkModule(Const.BASE_URL))
                .build();
    }

    public static NetworkComponent getNetworkComponent() {
        return sNetworkComponent;
    }
}