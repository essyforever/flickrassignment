package eman.ca.flickrassignment.utils;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.View;

import eman.ca.flickrassignment.R;

/**
 * Created by Esmond on 2018-09-14.
 */

public class SnackBarHelper {
    /**
     * Shows a red snackbar indicating an error.
     *
     * @param parentView
     * @param error
     */
    public static Snackbar showError(@NonNull View parentView, @NonNull String error, String actionText, View.OnClickListener action) {
        if (parentView == null || parentView.getContext() == null || parentView.getContext().getResources() == null) {
            return null;
        }

        return showSnackbar(parentView, error, parentView.getContext().getResources().getColor(R.color.red), actionText, Color.WHITE, action);
    }

    /**
     * Shows a snack bar.
     *
     * @param parentView
     * @param message
     * @param actionText
     * @param action
     */
    public static Snackbar showSnackbar(@NonNull View parentView, @NonNull String message, String actionText, View.OnClickListener action) {
        Snackbar snackbar = Snackbar.make(parentView, message, Snackbar.LENGTH_LONG);

        if (actionText != null && action != null) {
            snackbar.setDuration(Snackbar.LENGTH_INDEFINITE);
            snackbar.setAction(actionText, action);
        }
        snackbar.show();

        return snackbar;
    }

    /**
     * Shows a snack bar.
     *
     * @param parentView
     * @param message
     * @param backgroundColor
     * @param actionText
     * @param actionTextColor
     * @param action
     */
    public static Snackbar showSnackbar(@NonNull View parentView, @NonNull String message, int backgroundColor, @Nullable String actionText, int actionTextColor, @Nullable View.OnClickListener action) {
        Snackbar snackbar = Snackbar.make(parentView, message, Snackbar.LENGTH_LONG);

        if (actionText != null && action != null) {
            snackbar.setDuration(Snackbar.LENGTH_INDEFINITE);
            snackbar.setActionTextColor(actionTextColor);
            snackbar.setAction(actionText, action);
        }

        snackbar.getView().setBackgroundColor(backgroundColor);
        snackbar.show();

        return snackbar;
    }
}
