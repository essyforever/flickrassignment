package eman.ca.flickrassignment.view.viewholder;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import eman.ca.flickrassignment.R;

/**
 * Created by Esmond on 2018-09-14.
 */

public class FlickrViewHolder extends RecyclerView.ViewHolder implements FlickrRowView {

    @BindView(R.id.thumbnail)
    ImageView mThumbNailImg;
    @BindView(R.id.title)
    TextView mTitle;
    @BindView(R.id.date_taken)
    TextView mDate;
    @BindView(R.id.author)
    TextView mAuthor;
    @BindView(R.id.tags)
    TextView mTags;


    public FlickrViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void setImage(String url) {
        Picasso.get().load(url).error(R.mipmap.ic_launcher).into(mThumbNailImg);
    }

    @Override
    public void setTitle(String title) {
        mTitle.setText(title);
    }

    @Override
    public void setDate(String date) {
        mDate.setText(date);
    }

    @Override
    public void setAuthor(String author) {
        mAuthor.setText(author);
    }

    @Override
    public void setTags(String tags) {
        mTags.setText(mTags.getResources().getString(R.string.tags, tags));
    }
}
