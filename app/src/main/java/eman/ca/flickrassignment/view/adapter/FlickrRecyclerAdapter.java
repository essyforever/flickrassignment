package eman.ca.flickrassignment.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import eman.ca.flickrassignment.R;
import eman.ca.flickrassignment.view.viewholder.FlickrViewHolder;

/**
 * Created by Esmond on 2018-09-14.
 */

public class FlickrRecyclerAdapter extends RecyclerView.Adapter<FlickrViewHolder> {

    private final FlickrListPresenter mFlickrListPresenter;

    public FlickrRecyclerAdapter(FlickrListPresenter flickrListPresenter) {
        mFlickrListPresenter = flickrListPresenter;
    }

    @NonNull
    @Override
    public FlickrViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FlickrViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_flickr_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FlickrViewHolder holder, int position) {
        mFlickrListPresenter.onBindRowViewAtPosition(position, holder);
    }

    @Override
    public int getItemCount() {
        return mFlickrListPresenter.getRowsCount();
    }
}
