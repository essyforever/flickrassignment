package eman.ca.flickrassignment.view.viewholder;

/**
 * Created by Esmond on 2018-09-14.
 */

public interface FlickrRowView {

    void setImage(String url);
    void setTitle(String title);
    void setDate(String date);
    void setAuthor(String author);
    void setTags(String tags);
}
