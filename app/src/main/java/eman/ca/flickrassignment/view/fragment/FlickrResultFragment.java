package eman.ca.flickrassignment.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ViewAnimator;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import eman.ca.flickrassignment.FlckrApplication;
import eman.ca.flickrassignment.R;
import eman.ca.flickrassignment.model.ResultList;
import eman.ca.flickrassignment.network.FlickrApi;
import eman.ca.flickrassignment.utils.SnackBarHelper;
import eman.ca.flickrassignment.view.adapter.FlickrListPresenter;
import eman.ca.flickrassignment.view.adapter.FlickrRecyclerAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Esmond on 2018-09-14.
 */

public class FlickrResultFragment extends Fragment implements Callback<ResultList> {

    public static FlickrResultFragment newInstance() {
        
        Bundle args = new Bundle();
        
        FlickrResultFragment fragment = new FlickrResultFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    @BindView(R.id.viewAnimator)
    ViewAnimator mViewAnimator;

    @Inject
    FlickrApi mFlickrApi;

    String mTag;
    Snackbar mSnackbar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_flickr_result, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FlckrApplication.getNetworkComponent().inject(this);

        ButterKnife.bind(this, getView());

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        executeSearch();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (getActivity() == null) {
            return;
        }
        inflater.inflate(R.menu.searchbar, menu);
        initSearchView(menu.findItem(R.id.menu_action_search));
    }

    private void initSearchView(final MenuItem searchItem) {
        if (searchItem != null) {
            SearchView searchView = (SearchView) searchItem.getActionView();
            if (searchView != null) {
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String queryString) {
                        searchItem.collapseActionView();
                        mTag = queryString;
                        executeSearch();
                        return true;
                    }

                    @Override
                    public boolean onQueryTextChange(String s) {
                        return false;
                    }
                });
            }
        }
    }

    private void executeSearch() {
        mViewAnimator.setDisplayedChild(0);
        Call<ResultList> resultListCall = mFlickrApi.getPublicPhotos(mTag);
        resultListCall.enqueue(this);
    }

    @Override
    public void onResponse(Call<ResultList> call, Response<ResultList> response) {
        if (response.isSuccessful()) {
            mRecyclerView.setAdapter(new FlickrRecyclerAdapter(new FlickrListPresenter(response.body())));
            mViewAnimator.setDisplayedChild(1);
        } else {
            onResponseFailure(getString(R.string.error_network_please_try_again));
        }
    }

    @Override
    public void onFailure(Call<ResultList> call, Throwable t) {
        onResponseFailure(getString(R.string.error_network_please_try_again));
    }

    private void onResponseFailure(String errorMessage) {
        mViewAnimator.setNextFocusDownId(1);
        showSnackbarError(errorMessage);
    }

    /**
     * Display Snackbar with errorMessage and a button option to Retry the Api
     * @param errorMessage
     */
    private void showSnackbarError(String errorMessage) {
        mSnackbar = SnackBarHelper.showError(getView(), errorMessage, getString(R.string.retry), generateSnackBarOnClickListener());
    }

    private View.OnClickListener generateSnackBarOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeSearch();
            }
        };
    }

}
