package eman.ca.flickrassignment.view.adapter;

import eman.ca.flickrassignment.model.FlickrPhoto;
import eman.ca.flickrassignment.model.ResultList;
import eman.ca.flickrassignment.view.viewholder.FlickrRowView;

/**
 * Created by Esmond on 2018-09-14.
 */

public class FlickrListPresenter {

    private final ResultList mResultList;

    public FlickrListPresenter(ResultList resultList) {
        mResultList = resultList;
    }

    public void onBindRowViewAtPosition(int position, FlickrRowView rowView) {
        FlickrPhoto flickrPhoto = mResultList.getFlickrPhoto(position);
        if(flickrPhoto.getMedia() != null) {
            rowView.setImage(flickrPhoto.getMedia().getMediaUrl());
        }
        rowView.setTitle(flickrPhoto.getTitle());
        rowView.setDate(flickrPhoto.getDateTaken());
        rowView.setAuthor(flickrPhoto.getAuthor());
        rowView.setTags(flickrPhoto.getTags());
    }

    public int getRowsCount() {
        return mResultList.getFlickrPhotoList() != null ? mResultList.getFlickrPhotoList().size() : 0;
    }
}
